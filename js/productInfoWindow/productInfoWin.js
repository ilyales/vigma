angular.module("productInfoWin", ['ngTasty','xeditable','ngDialog','ngFileUpload'])
.directive('productInfoWindow', ['$http','$window','ngDialog','Upload',function($http,$window,ngDialog,Upload) {            
	return {
		templateUrl : "/js/productInfoWindow/productInfoWindow.html",
		replace: true,
		scope:{
			windowshow:'=',
			product:'=',
			providers:'=',
			position:'=',
		},
		restrict: 'E',

		compile: function compile(templateElement, templateAttrs) {
                return {
                    pre: function (scope, element, attrs, controller) {
                        scope.allProductsSearch = {
							getResource : function (params, paramsObj) {
								var urlApi = '/index.php?r=productsApi/getProductsToCatalogAjax&showHidden=0&' + params; // showHidden=0 не показывать скрытые
								return $http.get(urlApi).then(function (response) {
									//составим список синонимов для отбражения      
									return {
										 'rows': response.data.rows,
										 'header': response.data.header,
										 'pagination': response.data.pagination,
										 'sortBy': response.data['sort-by'],
										 'sortOrder': response.data['sort-order']
										}
									});
							},

							initTasty : {
								'count': 50,
								'page': 1,
								'sortBy': 'name',
								'sortOrder': 'dsc'
							},

							
							itemsPerPage: 50,
							listItemsPerPage:[50,100, 500, 1000],

							filter:{
								name:'',
								chod:'',
							},

							clearFilter: function(type)
							{
								switch (type) {
									case 'name':
										scope.allProductsSearch.filter.name = "";
										break;
									case 'chod':
										scope.allProductsSearch.filter.chod = "";
										break;
								}
							},

							visible:false,

							show:function() 
							{
								scope.allProductsSearch.visible = !scope.allProductsSearch.visible;
							},

							selectProductToCopy : function(product)
							{
								var url='/index.php?r=productsApi/copyProductInfoFromTotAjax';
								$http.post(url,{fromProductId:scope.product.id,
												toProductId:product.id,
											}).success(function (data) {
									if (data!="false")
									{
										alert('Скопировано!');	
										scope.allProductsSearch.visible = false;							
									}   
									else
									{
										alertDanger('Не удалось скопировать!');
									}  
								});
							},
						};

						
                    },

                    post: function (scope, element, attrs, controller) {
                        scope.productInfoWindowWatcher=null;
						scope.newProvider= {
							id:1,
						}

						//следим за значение windowshow, когда станет true запускаем loadProductsInfoWin() для инициализации окна
						scope.productInfoWindowShowWatcher = scope.$watch('windowshow',function(newValue, oldValue) {
							if (!angular.equals(oldValue, newValue))
								{
									if (scope.windowshow==true && scope.product!=null) scope.reloadProductInfo();			 
								}
						},false);

						scope.loadProductsInfoWin=function()
						{

							var windowTopOffset = $window.pageYOffset +20;
							
							element.css('top',windowTopOffset+'px');
							if (scope.product.comment==null || scope.product.comment=="")
							{
								angular.element(document.getElementById('piw_comment-text')).removeClass('piw_comment-text__full');
							}
							else 
							{
								angular.element(document.getElementById('piw_comment-text')).addClass('piw_comment-text__full');
							}



							scope.allProductsSearch.visible = false;
							

							//из поля size вытащим длину, ширину, высоту (они разделены ";")
							if (scope.product.size!=null)
							{
								var sizes = scope.product.size.split(';');
								scope.product.sizeL=sizes[0];
								scope.product.sizeW=sizes[1];
								scope.product.sizeH=sizes[2];
								scope.product.sizeSpace = scope.product.sizeL*scope.product.sizeW*scope.product.sizeH*0.001;
							}
							else
							{
								scope.product.sizeL=0;
								scope.product.sizeW=0;
								scope.product.sizeH=0;
								scope.product.sizeSpace = 0;
							}

							scope.product.sizeSpace = new Decimal(scope.product.sizeSpace);

							// if (scope.product.sizeSpace!=null && typeof(scope.product.sizeSpace)!='undefined') scope.product.sizeSpace = new Decimal(scope.product.sizeSpace);
							// else scope.product.sizeSpace = new Decimal(0);


								


							if (scope.product.weight!=null && typeof(scope.product.weight)!='undefined') scope.product.weight = new Decimal(scope.product.weight);
							else scope.product.weight = new Decimal(0);

							

							if (scope.position!=null && typeof(scope.position)!='undefined')
							{
								scope.position.sizeSum = scope.product.sizeSpace.mul(scope.position.count);
								scope.position.sizeSumDisplay = scope.position.sizeSum.toFixed(3);
								scope.position.weightSum = scope.product.weight.mul(scope.position.count);
								scope.position.weightSumDisplay = scope.position.weightSum.toFixed(3);
							}
							
							if (scope.product.price_chtz==null) scope.product.price_chtz=0;

							//преобразуем даты отсатков в русский формат
							var month = ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'];
							for (var i=0;i<scope.product.stocks.length;i++)
							{
								if (scope.product.stocks[i].date!=null)
								{
									var t = scope.product.stocks[i].date.split(/[- :]/);
									scope.product.stocks[i].dateDisplay = t[2] + ' ' + month[parseInt(t[1])-1];
								}								
							}



							//пометим поставщика по-умолчанию и сопоставим ЧОД/НАЗВААНИЕ с поставщиками (по inputRequest.provider_id).
							//
							//provProd, которому соответствует inputRequest c original_item=1 (т.е. это поставщик с которым создавался товар), пометим required - и его нельзя будет удалить
							//
							for (var i=0;i<scope.product.provProds.length;i++)
							{
								

								if (scope.product.provProds[i].price_date==null) scope.product.provProds[i].price_date=0;
								else
								{
									var t = scope.product.provProds[i].price_date.split(/[- :]/);
									scope.product.provProds[i].price_date = t[2]+'-'+t[1]+'-'+t[0];
								}
																	

								

								if (scope.product.provProds[i].id==scope.product.defaultprovprod_id)
								{
									scope.product.provProds[i].defaultProvider=true;
								}
								else
								{
									scope.product.provProds[i].defaultProvider=false;
								}
								
								scope.product.provProds[i].required = false;

								for (var j=0;j<scope.product.inputRequests.length;j++)
								{
									if (scope.product.inputRequests[j].provprod_id == scope.product.provProds[i].id)
									{
										scope.product.provProds[i].provName = scope.product.inputRequests[j].name;
										scope.product.provProds[i].provChod = scope.product.inputRequests[j].chod_display;
										//если inputRequest хранит оригинальное название и чод, то такого поставщика запрещено удалять, иначе удалится этот
										//inputRequest, что не допустимо
										if (scope.product.inputRequests[j].original_item==1)
										{
											scope.product.provProds[i].required = true;
										}

									}
								}

								//так же если срок поставки=null поставим его как 0, что бы можно было редактировать
								if (scope.product.provProds[i].delivery_time==null) scope.product.provProds[i].delivery_time=0;
							}

							$('#productInfoWindow').draggable({
								 handle: "#piw_windowHead"
							});
						};


						scope.reloadProductInfo = function()
						{
							var url='/index.php?r=productwinApi/getProductInfoAjax';
							$http.post(url,{
								product_id:scope.product.id,
							}).success(function (data) {
								 if (data!="false")
								 {
								 	scope.product = data;
								 	if (scope.product.price_chtz!=null && typeof(scope.product.price_chtz)!='undefined') scope.product.price_chtz = new Decimal(scope.product.price_chtz);
									else scope.product.price_chtz = new Decimal(0);
									scope.product.price_chtzDispaly = scope.product.price_chtz.toFixed(2);

								 	scope.loadProductsInfoWin();							
								 }   
								 else
								 {
								 	alert('Не удалось обновить информацию о товаре!');
								 }  
							});
						};

						scope.closeProductsInfoWin=function() {
					 		scope.windowshow = false;
							
						};

						scope.uploadImg = function(file) {
							Upload.upload({
						        url: '/index.php?r=productwinApi/uploadImgAjax',
						        data: {
						        	file: file,
						        	productId:scope.product.id,
						        }
						    }).then(function (resp) {
						    	if (resp.data.status)
						    	{
						    		scope.product.img = resp.data.img;
						    		if (scope.position!=null) scope.position.product.img = resp.data.img;
						    	}
						   		
						    }, function (resp) {
						       alert('Ошибка загрузки!');

						    }, function (evt) {
						        // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						        // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
						     });
						}

						scope.deleteImg = function()
						{
							scope.showConfirm("Удалить изоражение?").then(function(){
					 			var url='/index.php?r=productwinApi/deleteImgAjax';
								$http.post(url,{
									productId: scope.product.id
								}).success(function (data) {
									if (data.status)
									{
										scope.product.img = null;
										if (scope.position!=null) scope.position.product.img = null;
									}
									else
									{
										alert("При удалении изображения возникла ошибка!");
									}
								});	
					 		});
						}

						scope.saveCommentBtnDisable = true;

						scope.commentChanged=function() {
							scope.saveCommentBtnDisable = false;
						}

						scope.saveComment=function() {
							
							//scope.product.comment = document.getElementById('piw_comment-text').value;
							var url='/index.php?r=productwinApi/updateCommentToProductAjax';
							$http.post(url,{productId:scope.product.id, text:scope.product.comment}).success(function (data) {
								if (data!='false') scope.saveCommentBtnDisable = true;  
							});
						}

						scope.updateInputRequest=function(inputRequest){
					 		var url='/index.php?r=productwinApi/updateInputRequestAjax';
							$http.post(url,{id:inputRequest.id, name:inputRequest.name, chod_display:inputRequest.chod_display}).success(function (data) {
									////console.log(data);        
							});
					 	};

					 	scope.deleteInputRequest=function(inputRequest) {
					 		scope.showConfirm("Удалить синоним: " + inputRequest.name + ' ' + inputRequest.chod +" ?").then(function(){
						 		var url='/index.php?r=productwinApi/deleteInputRequestAjax';
						 		$http.post(url,{id:inputRequest.id}).success(function (data) {
									////console.log(data);  
									if (data!=false)   
									{
										for (var i=0;i<scope.product.inputRequests.length;i++)
										{
											if (scope.product.inputRequests[i].id == inputRequest.id)
											{
												scope.product.inputRequests.splice(i,1);
												break;
											}

										}
									}  
								});
							});
					 	};

					 	scope.addInputRequset=function(){
					 		var url='/index.php?r=productwinApi/addInputRequestAjax';
							$http.post(url,{name:scope.newInputName, chod_display:scope.newInputChod, product_id:scope.product.id}).success(function (data) {
								 
								 if (data!="false")
								 {
									scope.product.inputRequests.push(data);
									scope.newInputName="";
									scope.newInputChod="";
								 }     
							});
					 	};

					 	scope.addProvider=function(){
					 		if (scope.newProvider.price_in=="" || scope.newProvider.price_in==null)
					 		{
					 			alert('Цена входящая - обязательное поле!');
					 			return;
					 		} 
					 		if (scope.newProvider.price_orig=="" || scope.newProvider.price_orig==null) scope.newProvider.price_orig=scope.newProvider.price_in;
					 		var now = new Date();
					 		var month = now.getMonth()+1;
					 		var now_str = now.getFullYear() + '-' + month + '-' + now.getDate();

					 		var url='/index.php?r=productwinApi/attachProviderToProductAjax';
							$http.post(url,{provider_id:scope.newProvider.id,
											price_in:scope.newProvider.price_in,
											price_orig:scope.newProvider.price_orig,
											price_date:now_str,
											delivery_time:scope.newProvider.delivery_time,
											product_id:scope.product.id,
											provName:scope.newProvider.provName,
											provChod:scope.newProvider.provChod,
										}).success(function (data) {
								 if (data!="false")
								 {
								 	data.defaultProvider = false;
								 	// data.price_in = new Decimal(data.price_in);
								 	// data.price_orig = new Decimal(data.price_orig);
								 	var t = data.price_date.split(/[- :]/);
									
									data.price_date = t[2]+'-'+t[1]+'-'+t[0];	

									scope.product.provProds.push(data);
									scope.newProvider= {
										id:1,
									}									
								 }   
								 else
								 {
								 	alertDanger('Не удалось прикрепить поставщика к товару!');
								 }  
							});
					 	};

					 	scope.deleteProvProd=function(provProd) {
					 		scope.showConfirm("Открепить поставщика " + provProd.provider_name + " от товара?").then(function(){
						 		var url='/index.php?r=productwinApi/deleteProvProdAjax';
								$http.post(url,{id:provProd.id}).success(function (data) {
									if (data!="false")
									{
										for (var i=0;i<scope.product.provProds.length;i++)
										{
											if (scope.product.provProds[i].id == provProd.id)
											{
												scope.product.provProds.splice(i,1);
												break;
											}

										}					
									} 
									else
									{
										alertDanger('Не удалось удалить связь с поставщиком!');
									}
								});
					 		});
					 	};

					 	scope.changeDefaultProvider=function(provProd)
					 	{
					 		var url='/index.php?r=productwinApi/changeDefaultProvidrForProductAjax';
							$http.post(url,{provprod_id:provProd.id,product_id:scope.product.id}).success(function (data) {
								 if (data!="false")
								 {
									scope.product.defaultprovprod_id = provProd.id;
									
								 }   
								 else
								 {
								 	alertDanger('Не удалось сменить поставщика по-умолчпнию!');
								 }  
							});
					 	};

					 	scope.updateProvProd=function(provProd)
					 	{
					 		
					 		provProd.price_in = provProd.price_in.toString().replace(",", "."); 
					 		provProd.price_orig = provProd.price_orig.toString().replace(",", "."); 
					 							 		

					 		//проверим, не выбран ли этот поставщик в качестве поставщика для позиции
					 		if (scope.position!=null && scope.position.provider!=null && scope.position.provider.id==provProd.provider_id) {
					 			scope.position.provider.price_in=new Decimal(provProd.price_in);
					 			
								scope.position.changePriceInSum(scope.position); // пересчитать позицию
					 		}

					 		var url='/index.php?r=productwinApi/updateProvProdAjax';
							$http.post(url,{id:provProd.id, price_in:provProd.price_in, price_orig:provProd.price_orig, price_date:provProd.price_date, delivery_time:provProd.delivery_time}).success(function (data) {
								 if (data!="false")
								 {
														
								 }   
								 else
								 {
								 	alertDanger('Не удалось обновить информацию!');
								 }  
							});
					 	};

					 	//обновить ориг. назва, ориг чод, код товара, прайс чтз, вес
					 	scope.updateProductInfo = function()
					 	{
					 		scope.product.price_chtz= scope.product.price_chtz.toString().replace(",", "."); 
					 		scope.product.weight = scope.product.weight.toString().replace(",", "."); 

					 		var url='/index.php?r=productwinApi/updateProductInfoAjax';
							$http.post(url,{product_id: scope.product.id,
											original_name: scope.product.original_name,
											chod_display: scope.product.chod_display,
											catalog_name: scope.product.catalog_name,
											catalog_chod: scope.product.catalog_chod,
											product_num: scope.product.product_num,
											price_chtz:scope.product.price_chtz,
											weight: scope.product.weight,

							}).success(function (data) {
								 if (data!="false")
								 {
									scope.product.price_chtz = new Decimal(scope.product.price_chtz);
									scope.product.weight = parseFloat(scope.product.weight);
								 }   
								 else
								 {
								 	alert('Не удалось обновить информацию!');
								 }  
							});
					 	};					 	

						scope.updateProductSize = function()
						{
							if (scope.position!=null)
							{
								scope.position.product.size = scope.product.sizeL + ';' + scope.product.sizeW + ';' + scope.product.sizeH;

								var sizeSpace = scope.product.sizeL*scope.product.sizeW*scope.product.sizeH*0.001;
								
								if (isFinite(sizeSpace)) scope.position.product.sizeSpace = new Decimal(sizeSpace);
								else scope.position.product.sizeSpace = new Decimal(0);

								
								scope.position.sizeSum = scope.position.product.sizeSpace.mul(scope.position.count);
								scope.position.sizeSumDisplay = scope.position.sizeSum.toFixed(3);
								
								scope.position.calcSizeTotal();
							}

							scope.product.size = scope.product.sizeL + ';' + scope.product.sizeW + ';' + scope.product.sizeH;

							var url='/index.php?r=productwinApi/updateProductSizeAjax';
							$http.post(url,{product_id:scope.product.id,size: scope.product.size}).success(function (data) {
								 
								 if (data!="false")
								 {
														
								 }   
								 else
								 {
								 	alertDanger('Не удалось обновить информацию!');
								 }  
							});
						};

						scope.hideProduct = function()
						{
							var hidden;
					    	if (scope.product.hidden==1) hidden=0;
					    	else hidden=1;
					    	var url = '/index.php?r=productsApi/hideProductAjax';
							$http.post(url,{
								product_id:scope.product.id,
								hidden:hidden,
							}).success(function (data) {
								if (data=='false')
								{
									alert('Ошибка!');
								}
								else
								{
									scope.product.hidden=hidden;
								}
							});
						};

						scope.deliveryComment = {
							visible:false,
							provProd:null,

							show:function(provProd,event) 
							{
								// console.log(event); //+130
								// //angular.element('#element').css('height', '100px');
								// var position = event.target.getBoundingClientRect();
								// var x = position.left;
								// var y = position.top
								// console.log();piw_providerTableContent
								// var top = event.layerY + 130;
								// angular.element(document.querySelector( '#piw_deliveryComment' )).css('top',top+'px');
								scope.deliveryComment.visible = true;
								scope.deliveryComment.provProd = provProd;
							},

							close:function()
							{
								scope.deliveryComment.visible = false;
							},

							save:function()
							{
								var url = '/index.php?r=productwinApi/updateDeliveryCommentAjax';
								$http.post(url,{
									id:scope.deliveryComment.provProd.id,
									deliveryComment:scope.deliveryComment.provProd.delivery_comment,
								}).success(function (data) {
									if (data=='false')
									{
										alert('Ошибка!');
									}
									else
									{
										scope.deliveryComment.visible = false;
									}
								});
							},

							delete:function()
							{
								var url = '/index.php?r=productwinApi/updateDeliveryCommentAjax';
								$http.post(url,{
									id:scope.deliveryComment.provProd.id,
									deliveryComment:null,
								}).success(function (data) {
									if (data=='false')
									{
										alert('Ошибка!');
									}
									else
									{
										scope.deliveryComment.provProd.delivery_comment = null;
										scope.deliveryComment.visible = false;
									}
								});
							}


						};

						scope.stockEdit = {
							visible:false,
							stock:null,
							originalStock:null,
							show:function(stock)
							{
								scope.stockEdit.stock = angular.copy(stock);
								//scope.stockEdit.originalChodname = stock.chodname;
								scope.stockEdit.originalStock = stock;
								scope.stockEdit.visible = true;
							},
							close:function()
							{
								scope.stockEdit.visible = false;
							},

							save:function()
							{	
								if (scope.stockEdit.stock.chodname==scope.stockEdit.originalStock.chodname) 
								{
									console.log("значение не поменялось");
									scope.stockEdit.close();
									return;
								}

								var url = '/index.php?r=productwinApi/updateStockChodnameAjax';
								$http.post(url,{
									id:scope.stockEdit.stock.id,
									chodname:scope.stockEdit.stock.chodname,
								}).success(function (data) {
									if (data.status==false)
									{
										alert('Ошибка при сохранении!');
									}
									else
									{
										scope.stockEdit.originalStock.chodname = scope.stockEdit.stock.chodname;
										scope.stockEdit.close();
									}
								});
							},

							delete:function()
							{

								scope.showConfirm("Удалить связь товара с остатком?").then(
					 				function(){
					 					var url = '/index.php?r=productwinApi/deleteStockAjax';
										$http.post(url,{
											id:scope.stockEdit.stock.id,
										}).success(function (data) {
											if (data.status==false)
											{
												alert('Ошибка при удалении!');
											}
											else
											{
												for (var i=0;i<scope.product.stocks.length;i++)
												{
													if (scope.product.stocks[i].id==scope.stockEdit.stock.id)
													{
														scope.product.stocks.splice(i,1);
														break;
													}
												}
												

												console.log(scope.product.stocks);
												scope.stockEdit.close();
											}
										});
					 				}			
					 			);								
							},

						};

						scope.showConfirm = function(message) {
					    	scope.showConfirmMessage = message;

					 		return	ngDialog.openConfirm({
					 				template: 'confirmTemplate.html',
								    className: 'ngdialog-theme-plain',
								    closeByDocument:false,
								    closeByEscape:false,
								    showClose:false,
								    scope: scope,
								});
					    }
					}
                    
                }
            }
		
	}
}]); 




