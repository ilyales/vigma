angular.module('groups').controller('groupProductsController', function($scope,$http, ngDialog) {
    
    $scope.init = function() {
        var nonGroup = {
            id:null,
            group_num:"Не привязан к группе",
            name:"Не привязан к группе"
        }
        groups.splice(0,0,nonGroup);
        $scope.groups.init(groups); 
        $scope.providers =  providers;
    }

    $scope.groups = {
        init:function(groups) {
            this.items = groups;
            this.selected = this.items[0];
        },
        changed:function() {
            $scope.products.filter.groupId = this.selected.id;            
        }
    }

    $scope.products = {
        initTasty:{
            'count': 100,
            'page': 1,
            'sortBy': 'name',
            'sortOrder': 'dsc'
        },
        filter: {
            name:'',
            chod:'',
            groupId:null,
        },
        itemsPerPage : 100,
        listItemsPerPage : [50,100, 500, 1000],
        dataLoading : false, 

        getResource:function (params, paramsObj) {
            $scope.products.dataLoading = true;
            var urlApi = '/index.php?r=groupsApi/getPtoductsByGroupAjax&' + params; 
            return $http.get(urlApi).then(function (response) {
                $scope.products.dataLoading = false;     
                $scope.products.rows = response.data.rows;
                return {
                     'rows': $scope.products.rows,
                     'header': response.data.header,
                     'pagination': response.data.pagination,
                     'sortBy': response.data['sort-by'],
                     'sortOrder': response.data['sort-order']
                    }
                });
        },

        clearFilter:function(type) {
            switch (type) {
                case 'name':
                    $scope.products.filter.name = "";
                    break;
                case 'chod':
                    $scope.products.filter.chod = "";
                    break;
            }
        },

        setProductRowActive:function(product) {
            for (var i=0;i<this.rows.length;i++)
            {
                this.rows[i].active = false;
            }
            product.active = true;
        },

        showGroupEditor:function(product,rowIndex) {
            this.groupEditorProductIndex = rowIndex;
            this.groupEditorTopOffset = rowIndex*31;
            this.groupEditorProduct = angular.copy(product);
        },

        updateProductGroups:function(groups_ids,groups_str) {
            console.log(groups_ids,groups_str);
            this.groupEditorProduct.groups_ids = groups_ids;
            this.groupEditorProduct.groups_str = groups_str;
            this.rows[this.groupEditorProductIndex].groups_ids = groups_ids;
            this.rows[this.groupEditorProductIndex].groups_str = groups_str;
        }
    }

    $scope.productInfoWindow = {
        product:null,
        show:false,
        showProductsInfoWin:function(product)
        {
            console.log('fff');
            $scope.productInfoWindow.product = product;
            $scope.productInfoWindow.show = true;
            //$scope.setProductRowActive(product);
        },
    }
});