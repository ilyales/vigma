angular
  .module('AllRequisitions')
  .factory('RequisitionsService', RequisitionsService);

RequisitionsService.$inject = ['$http','$q'];

function RequisitionsService($http,$q) {
  var service = {
    getRequisitions:getRequisitions,
    deleteRequisition:deleteRequisition,
  }
  return service;

  function getRequisitions(status,count,offset) {
    var url = "/index.php?r=requisitionApi/getRequisitionsAjax"
    var deferred = $q.defer();
    $http.post(url,{
      status:status,
      count:count,
      offset:offset
    }).then(
      function(rawData){
        var data = rawData.data;
        if (data.status) {
          deferred.resolve(data.requisitions);
        }
        else {
          deferred.reject();
        }
      },
      function() {
        deferred.reject();
      }
    );
    return deferred.promise;
  }

  function deleteRequisition(requisitionId) {
    var url = "/index.php?r=requisitionApi/deleteRequisitionAjax"
    var deferred = $q.defer();
    $http.post(url,{
      id:requisitionId,
    }).then(
      function(rawData){
        var data = rawData.data;
        if (data.status) {
          deferred.resolve();
        }
        else {
          deferred.reject();
        }
      },
      function() {
        deferred.reject();
      }
    );
    return deferred.promise;
  }
}