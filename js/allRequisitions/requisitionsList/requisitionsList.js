angular.module('AllRequisitions')
	.component('requisitionsList', {
	    controller:requisitionsList,
	    controllerAs: 'vm',
	    templateUrl:'/js/allRequisitions/requisitionsList/requisitionsList.html',
      bindings: {
        title: '@',
        requisitions: '<',
        totalItems: '<',
        status: '@',
      }
	  });

requisitionsList.$inject = ['$scope','RequisitionsService','ngDialog'];

function requisitionsList($scope,RequisitionsService,ngDialog) {
  var vm=this; 
  vm.changePage = changePage;
  vm.deleteRequisition = deleteRequisition;
  
  function changePage(count,offset) {
    RequisitionsService.getRequisitions(vm.status,count,offset).then(
      function(requisitions){
        vm.requisitions = requisitions;
      }
    );
  }

  function deleteRequisition(requisitionId) {
    showConfirm("Удалить заявку?").then(function() {
      RequisitionsService.deleteRequisition(requisitionId).then(
        function(){
          for (var i=0;i<vm.requisitions.length;i++) {
            if (vm.requisitions[i].id===requisitionId) {
              vm.requisitions.splice(i, 1);
              break;
            }
          }
        }
      );
    });
  }

  function showConfirm (message) {
    $scope.showConfirmMessage = message;

    return  ngDialog.openConfirm({
        template: 'confirmTemplate.html',
          className: 'ngdialog-theme-plain',
          closeByDocument:false,
          closeByEscape:false,
          showClose:false,
          scope: $scope,
      });
    }
}

