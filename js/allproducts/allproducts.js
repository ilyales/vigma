var allProductsApp=angular.module('allProducts', ['ngTasty','productInfoWin']);

allProductsApp.directive('coincidentInputRequsets', function() {        
	return {
		template:"<div id='direc' class='coincidentInputRequsets'> {{output}}</div>",
		replace: true,
		scope:{
			items:'=',
			field:'@',
			value:'=',

		},
		restrict: 'E',
		link: 
			function (scope, element, attrs) {
					
					scope.output='';
					
					if (typeof scope.value != 'undefined')
					{
						
						for (var i=0;i<scope.items.length;i++)
						{
							
							//внесем в output только те inputRequests, в которых присутствует искомое значение, при условии что это не оригинальное название
							//(оно уже выведено)
							if (scope.items[i].original_item==0 && scope.items[i][scope.field]!=null && scope.items[i][scope.field].toLowerCase().indexOf(scope.value.toLowerCase())>-1 )
							{
								scope.output=scope.output + ' ' + scope.items[i][scope.field];

							}
						}
					}
      		},
	}
});


allProductsApp.controller('searchController', function($scope, $http) {
	// запрос данных 
	$scope.getResource = function (params, paramsObj) {
		var urlApi = '/index.php?r=productsApi/getProductsToCatalogAjax&showHidden=1&' + params; //showHideen = 1- отображать скрытые
		return $http.get(urlApi).then(function (response) {
			//составим список синонимов для отбражения      
			$scope.productsRows = response.data.rows;
			return {
				 'rows': $scope.productsRows,
				 'header': response.data.header,
				 'pagination': response.data.pagination,
				 'sortBy': response.data['sort-by'],
				 'sortOrder': response.data['sort-order']
				}
			});
	};

	$scope.initTasty={
		'count': 50,
		'page': 1,
		'sortBy': 'name',
		'sortOrder': 'dsc'
	};
	
	$scope.itemsPerPage = 50;
	$scope.listItemsPerPage = [50,100, 500, 1000];

	$scope.filter = {
		name:'',
		chod:'',
	};	

	$scope.providers =  providers;

	$scope.clearFilter = function(type)
	{
		switch (type) {
			case 'name':
				$scope.filter.name = "";
				break;
			case 'chod':
				$scope.filter.chod = "";
				break;
		}
	}

	$scope.setProductRowActive = function(product)
	{
		for (var i=0;i<$scope.productsRows.length;i++)
		{
			$scope.productsRows[i].product.active = false;
		}
		product.active = true;

	}	

	$scope.productInfoWindow = {
    	product:null,
    	show:false,
    	showProductsInfoWin:function(product)
    	{
    		console.log('fff');
    		$scope.productInfoWindow.product = product;
    		$scope.productInfoWindow.show = true;
    		//$scope.setProductRowActive(product);
    	},
    }

    $scope.newProduct = {
    	provider_id : 1,
    	visible:false,

    	original_name:"",
    	original_chod:"",

    	price_in:"",
    	price_chtz:"",
    	weight:"",

    	original_name_valid:true,
    	original_chod_valid:true,
    	price_in_valid:true,

    	show:function()
    	{
    		$scope.newProduct.visible = !$scope.newProduct.visible;
    	},

    	addProduct:function()
    	{
    		$scope.newProduct.original_name_valid = true;
    		$scope.newProduct.original_chod_valid = true;
    		$scope.newProduct.price_in_valid = true;

    		if ($scope.newProduct.original_name=="")
    		{
    			$scope.newProduct.original_name_valid = false;
    		} 

    		if ($scope.newProduct.original_chod=="")
    		{
    			$scope.newProduct.original_chod_valid = false;
    		} 

    		if ($scope.newProduct.price_in=="")
    		{
    			$scope.newProduct.price_in_valid = false;
    		} 

    		if (!$scope.newProduct.original_name_valid || !$scope.newProduct.original_chod_valid || !$scope.newProduct.price_in_valid)
    		{
    			return;
    		}

    		var size = $scope.newProduct.sizeL + ';' + $scope.newProduct.sizeW + ';' + $scope.newProduct.sizeH;
    		

    		
    		var price_chtz=parseFloat($scope.newProduct.price_chtz.replace(",","."));
			console.log(typeof price_chtz);
			if (!angular.isNumber(price_chtz)) {
				alert('Поле "Прайс ЧТЗ" должно быть числом');
				return;
			}

			var weight = null;

			if ($scope.newProduct.weight!=null)
			{
				weight=parseFloat($scope.newProduct.weight.replace(",","."));
				
				if (!angular.isNumber(weight)) {
					alert('Поле "Вес" должно быть числом');
					return;
				}
			}
    		
    		var price_in=parseFloat($scope.newProduct.price_in.replace(",","."));
			
			if (!angular.isNumber(price_in)) {
				alert('Поле "Цена" должно быть числом');
				return;
			}
			

    		var url = '/index.php?r=productsApi/addProductAjax';
			$http.post(url,{
				original_name:$scope.newProduct.original_name,
				original_chod:$scope.newProduct.original_chod,
				product_num:$scope.newProduct.product_num,
				price_chtz:price_chtz,
				weight:weight,
				size:size,
				provider_id:$scope.newProduct.provider_id,
				price_in:price_in,
				delivery_time:$scope.newProduct.delivery_time,

			}).success(function (data) {
				if (data=='false')
				{
					alert('Ошибка!');
				}
				else 
				{
					$scope.filter.name=$scope.newProduct.original_name;
					$scope.filter.chod=$scope.newProduct.original_chod;

					$scope.newProduct.original_name = "";
					$scope.newProduct.original_chod = "";
					$scope.newProduct.product_num = "";
					$scope.newProduct.price_chtz = "";
					$scope.newProduct.weight = "";
					$scope.newProduct.sizeL = "";
					$scope.newProduct.sizeW = "";
					$scope.newProduct.sizeH = "";
					$scope.newProduct.price_in = "";
					$scope.newProduct.delivery_time = "";
					$scope.newProduct.visible = false;
				}
			});
		
    	},
    }

    $scope.deleteProduct = function(product_id)
    {
    	if (!confirm('Удалить товар?')) {
			// Do nothing!
		} else {

			var url = '/index.php?r=productsApi/deleteProductAjax';
		    $http.post(url,{
				product_id:product_id,
				

			}).success(function (data) {
				if (data=='false')
				{
					alert('Ошибка!');
				}
				else if (data=='inRequisitions')
				{
					alert('Нельзя удалить т.к. товар находится в заявках.');
				}
				else 
				{
					//обновить содержимое таблицы
					// var currentName = $scope.filter.name; 
					// var currentChod = $scope.filter.chod; 
					// $scope.filter = {
					// 	name:'',
					// 	chod:'',
					// };

					// setTimeout(function(){
					// 	$scope.filter = {
					// 		name:currentName,
					// 		chod:currentChod,
					// 	};
					// },1000);
					alert('Товар удалён!');
				}
			});
		}
    }

    $scope.hideProduct = function(product)
    {
    	var hidden;
    	if (product.hidden==1) hidden=0;
    	else hidden=1;
    	var url = '/index.php?r=productsApi/hideProductAjax';
		$http.post(url,{
			product_id:product.id,
			hidden:hidden,
		}).success(function (data) {
			if (data=='false')
			{
				alert('Ошибка!');
			}
			else
			{
				product.hidden=hidden;
			}
		});
			
    }
});

