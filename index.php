<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('max_execution_time', 600); //300 seconds = 5 minutes
// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii-1.1.14.f0fee9/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
require_once(dirname(__FILE__).'/protected/extensions/phpmailer/phpmailer/PHPMailerAutoload.php');
Yii::createWebApplication($config)->run();
