
<div ng-app="myApp">

<div class="app-loading">
	<img class="app-loading_img" src="/images/loading.gif">
</div>

<div class="row requisition-title-row">	
	<div class="requisition-title">
		Заявка №
		<span class="requisition-title_number"><?php echo  $requisition_num ?></span>
		<?php if($requisition_from_catalog==1): ?>
			<span class="requisition-title_fromcatalog">(из каталога)</span>
		<?php endif; ?>
	</div>	
</div>



<div ng-controller="customerController" ng-init="init()" ng-cloak>

<div class="panel panel-default">
	<div class="panel-heading customer-panel-heading" ng-click="customer.changeVisible()">
	    <h3 class="panel-title customer-panel-title">Заказчик
	    	<span class="customer-panel-title-name" ng-show="!customer.visible && customer.requisitionCustomer==null"> - Нe выбран</span>
			<span class="customer-panel-title-name" ng-show="!customer.visible && customer.requisitionCustomer!=null"> - {{customer.requisitionCustomer.name}}</span>
	    </h3>
	    <img src="/images/down3.png" class="customer-panel-downimg">
	</div>
	<div class="panel-body customer-panel-body" ng-show="customer.visible">
		<div class="customer-search-line">
			<div class="customer-search">
				<input 
					type="text"
					class="customer-search_inp form-control" 
					ng-model="customer.customerListFilter.name"
					ng-focus="customer.showCustomerList()" 
					placeholder="Поиск по названию..."
				>
				<div class="customer-search-list"
				     ng-show="customer.customersListVisble" 
				     click-anywhere-but-here="customer.closeCustomerList()"
				     is-active='customer.customerListListenOutside'
				>
					<div class="customer-search-list_item"
						 ng-repeat="customerInfo in customer.customersList | filter:customer.customerListFilter | orderBy:'name'"
						 ng-click="customer.selectForRequisition(customerInfo)"
					>
						{{customerInfo.name}}
					</div>					
				</div>
			</div>

			<button class="btn btn-default customer-newCustBtn" ng-click="customer.newCustomerEdit()">Новый контрагент</button>
		</div>
	    
	    <div class="customer-current-line">
		    <div class="customer-current"
		         ng-class="{'customer-current__select':customer.requisitionCustomer!=null}"
		         ng-click="customer.showCustomerEdit()"
		    >
		    	<div class="customer-current_name" ng-show="customer.requisitionCustomer!=null">
			    	{{customer.requisitionCustomer.name}}    	
			    </div>
		    	<div class="customer-current_name" ng-show="customer.requisitionCustomer==null">
			    	Закачик не выбран   	
			    </div>
		    </div>
		    <img class="customer-unlink"
		         src="/images/delete.png"
		    	 ng-show="customer.requisitionCustomer!=null"
		    	 ng-click="customer.unlinkFromReauisition()"
		   	>
	    </div>
	</div>
</div>

<customeredit visible='customerEdit.visible' customer="customerEdit.customer" new-cust-save-cback="customer.addCustomer" statuses="customer.customerStatuses"></customeredit>

</div>  <!-- end customerController -->


<div ng-controller="positionsController" ng-init="loadRequisition();" ng-cloak>



<form class="form-horizontal" id="searchForm">
	<div class="form-group">
		<div class="col-xs-9"> 
			<div class="input-group">
				<?php $url=$this->createUrl('requisitionApi/searchTypeAjax').'&query='; ?>	
		      <angucomplete-alt id="currentInput"
	              placeholder="Наименование"
	              selected-object="selectedInput"
	              remote-url="<?php echo $url ?>"
	              remote-url-data-field="results"
	              title-field="name"
	              minlength="2"
	              input-class="form-control" 
	              override-suggestions="true"
	              ng-show="!positions.multipleSearch"
	              
	         >
	         </angucomplete-alt>
	         <textarea id="currentManyInput" class="form-control" rows="10" ng-model="positions.currentMultipleInput" ng-show="positions.multipleSearch"></textarea>
		      <span class="input-group-btn" style="vertical-align:top;">
		      	<div class="btn btn-default" ng-click="positions.switchMultipleSearch()" ng-class="{active:positions.multipleSearch==true}">
		        		<img width=20 src="/images/list2.png">
		        	</div>
		        	<button type="submit" class="btn btn-info" id="searchBtn" ng-click="positions.searchBtnClick()">Добавить</button>
		      </span>

		   </div>
		</div>
		
		<div class="col-xs-3"> 
			<button class="btn btn-default searchFromFileBtn" ngf-select="positions.searchFromFile($file)" >из файла</button>
		</div>

	</div>

<!-- 	<div class="form-group">
		<div class="col-xs-4">  
			
		</div>
	</div> -->

</form>

<div class="infoLine">

	
	<div class="btn-group positionsListBtns">
		  <button type="button" class="btn btn-default" ng-click="positionsList.changeListType('first')" ng-class="{'active':positionsList.listType=='first'}">Начальный список</button>
		  <button type="button" class="btn btn-default" ng-click="positionsList.createConfirmList()" ng-class="{'active':positionsList.listType=='start-confirm'}" ng-if="positionsList.confirmListNotActive"><img width=15 src="/images/confirm-blue.png" style="margin-top: -4px;"> Подтвердить список</button>
		  <button type="button" class="btn btn-default" ng-click="positionsList.changeListType('confirm')" ng-class="{'active':positionsList.listType=='confirm'}" ng-if="!positionsList.confirmListNotActive"><img width=15 src="/images/confirm-green.png" style="margin-top: -4px;"> Подтвержденный список</button>
		  <!--<button ng-click="positions.fixNums()">fix</button> -->
	</div>

	<div class="download-excel">
		<a href="/index.php?r=requisitionApi/exportRequisitionToXlsCustomer&requisition_id={{requisition_id}}&list_type={{positionsList.listType}}" title="Экспорт для заказачика"><img width=28 src="/images/excel.png"></a>
		<a href="/index.php?r=requisitionApi/exportRequisitionToXlsUA&requisition_id={{requisition_id}}&list_type={{positionsList.listType}}" title="Экспорт для ЮА"><img width=28 src="/images/excel-ua.png"></a>
		<a href="/index.php?r=requisitionApi/exportRequisitionToXlsInvoice&requisition_id={{requisition_id}}&list_type={{positionsList.listType}}" title="Экспорт для счета"><img width=28 src="/images/excel-invoice.png"></a>
	</div>
	
	<div class="set-prov-coef">	
		<select  class="form-control set-prov-coef_prov" ng-model="settingProviderCoef.providerId" ng-change="settingProviderCoef.changeProvider()">
			<option ng-repeat="provider in providers" ng-selected="provider.id==1"   value="{{provider.id}}">
				{{provider.name}}
			</option>
		</select>
		<input type="text" class="form-control set-prov-coef_coef" ng-model="settingProviderCoef.coef">
		<div class="btn btn-default set-prov-coef_btn" ng-click="settingProviderCoef.set()">ok</div>
	</div>
	<div class="requisitionTotals">
		<div class="requisitionTotals_row">
		<div class="requisitionTotalPriceIn">	
			Итог вх.: <span class="requisitionTotalPriceIn_price">{{positions.totalPriceInDisplay}} р. </span>
		</div>

		<div class="requisitionTotalPriceOut">	
			Итог иcх.: <span class="requisitionTotalPriceOut_price">{{positions.totalPriceOutDisplay}} р.</span>
		</div>

		<div class="requisitionTotalPriceSub">	
			Выруч.: <span class="requisitionTotalPriceSub_price">{{positions.totalPriceProcDisplay}} р.</span>
		</div>
		</div>
		<div class="requisitionTotals_row">
		<div class="requisitionTotalWeight">
			Вес: <span class="requisitionTotalWeight_weight">{{positions.totalWeightDisplay}} кг</span>
		</div>	
		<div class="requisitionTotalSize">
			Объём: <span class="requisitionTotalSize_size">{{positions.totalSize}} м3</span>
		</div>	
		</div>
	</div>
</div>


<div class="positionsTableHead" id="positionsTableHead">
	<div ng-click="positions.setOrder('position_num')" class="positionsTable_header positionsTable_header-id positionsTable_header__selected" >№</div>
	<div class="positionsTable_header positionsTable_header-photo" ></div>
	<div  ng-click="positions.setOrder('input')" class="positionsTable_header positionsTable_header-input" >Пользовательское название</div>
	<div ng-click="positions.setOrder('original_name')" class="positionsTable_header positionsTable_header-oroginalName" >Оригинальное название</div>
	<div ng-click="positions.setOrder('count',true)" class="positionsTable_header positionsTable_header-count">Кол-во</div>
	<div ng-click="positions.setOrder('product.price_chtz.toNumber()',true)" class="positionsTable_header positionsTable_header-priceChtz">прайс ЧТЗ</div>
	<div ng-click="positions.setOrder('provider.name')" class="positionsTable_header positionsTable_header-provider">Поставщик</div>
	<div ng-click="positions.setOrder('price_inSum.toNumber()',true)" class="positionsTable_header positionsTable_header-totalPriceIn">Cумма вх.</div>
	<div ng-click="positions.setOrder('price_coef.toNumber()',true)" class="positionsTable_header positionsTable_header-coef">Коэфф.</div>
	<div ng-click="positions.setOrder('price_out.toNumber()',true)" class="positionsTable_header positionsTable_header-priceOut">Цена исх.</div>
	<div ng-click="positions.setOrder('price_outSum.toNumber()',true)" class="positionsTable_header positionsTable_header-totalPriceOut">Cумма исх.</div>
	<div ng-click="positions.setOrder('weightSum.toNumber()',true)" class="positionsTable_header positionsTable_header-weight">Вес</div>
	<div  class="positionsTable_header positionsTable_header-size">Наличие</div>
	
</div>
<div class="positionsTable" id="positionsTable">
	<div class="positionsTable_row"
		 ng-repeat="position in positions.items | filter:positionsList.listType | orderBy:positions.order.orderValue"
		 ng-class="{'positionsTable_row__active':position.active!=null && position.active==true}" ng-click="positions.setPositionActive(position)"
	>
	 		<!-- таблица позиций - номер позиции -->
	 		<div class="positionsTable_num" >
	 			<span class="positionsTable_num-val" editable-text="position.position_num"
	 				onbeforesave="positions.changePositionNum(position,$data)">{{position.position_num}}.</span>
	 			
	 			<img class="positionsTable_pos-questio-status" src="/images/question-active.png" ng-show="position.questionForPositionStatus == 'active'">
	 			<img class="positionsTable_pos-questio-status" src="/images/confirm-green.png" ng-show="position.questionForPositionStatus == 'complite'">
	 		</div>

	 		<!-- таблица позиций - фото товара -->
	 		<div class="positionsTable_photo">

	 			<img class="positionsTable_photo-img"
	 				 ng-src="{{(position.product!=null && position.product.img!=null) ? position.product.img : '/images/nophoto.png'}}"
	 			>
	 			
	 			<!-- <img class="positionsTable_photo-img"  ng-show="position.product!=null && position.product.img!=null" ng-src="{{position.product.img}}">	 			
	 			<img class="positionsTable_photo-img"  ng-show="position.product==null || position.product.img==null" src="/images/nophoto.png"> -->	
	 		</div>

	 		<!-- таблица позиций - введенные названия -->
	 		<div class="positionsTable_input">
				{{position.input}} 
			</div>

			<!-- таблица позиций - информация о товаре -->
			<div class="positions-table_info">

				<div class="productInPositionRow">    
						     
					<div class="position_original-info">  
						<span class="productNotSelectLabel" ng-show="position.product.id==null">(выберите товар)</span>
						{{position.product.original_name}} &nbsp; {{position.product.chod_display}}
						<img class="unbindProductFromPosition" src="/images/null.png" ng-show="position.product.id!=null" ng-click="positions.unbindProductFromPosition(position)">
					</div>
					<div class="position_menu"> 
						<div class="position_menu-line">
							<img class="showQuestionForPosition" src="/images/question.png" ng-click="positions.showQuestionForPosition(position)">
							<img class="showLiveProductSearch" src="/images/search.png" ng-click="positions.showliveProductSearch(position)">
							<img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWinByPosition(position)" ng-show="position.product.id!=null">
						</div>
						<div class="position_menu-line">

						</div>
					</div>

					<input class="form-control position_count" ng-change="positions.changeCount(position)" ng-model="position.count"  value="1" type="text"> <span class="position_count_label">шт. </span>
					 
					<div class="position_price-block" ng-show="position.product.id!=null">
						
						<span class="position_priceChtz">
							{{position.product.price_chtzDispaly}}
							<span ng-show="position.product.price_chtz!=null"> р.</span>
							<span ng-show="position.product.price_chtz==null" class="position_notPriceChtz">нет</span>
						</span>
						<div class="position_provider-wrap">
							<div class="position_provider"
							     ng-click="positions.showProviderList(position)" 
							     ng-class="{position_provider__hasMany: position.product.provProds.length!=1,position_provider__notInPrice: position.provider.notInPrice}"
							>
								{{position.provider.name}}
							</div>
							<div class="position_provider-price"> {{thousands(position.provider.price_in.toFixed(2))}} р.</div>
						</div>
						
						<div class="form-control position_providers-list" ng-show="position.providersListVisible" ng-attr-size="{{position.product.provProds.length}}">
							<div class="position_providers-list-it"
									ng-repeat="provProd in position.product.provProds"
									ng-click="positions.changeProvder(position,provProd)"
									ng-class="{'position_providers-list-it__not':provProd.notInPrice==1}"
							>
								{{provProd.provider_name}} <span ng-show="provProd.notInPrice==1">(нет в прайсе)</span> - {{thousands(provProd.price_in)}} р.
							</div>
						</div>
						
						<span class="position_total-price-in">{{position.price_inSumDisplay}} р.</span>
						<input type="text" class="form-control position_price-coef" ng-model="position.price_coefDisplay"  ng-change="positions.changePriceCoef(position)" ng-class="{'position_price-coef__war':position.provider.min_price_coef!=null && position.price_coef.cmp(position.provider.min_price_coef)<0}">

						<input type="text" class="form-control position_price-out" ng-model="position.price_outDisplay" ng-change="positions.changePriceOut(position)" >
						<span class="position_price-out-label">р.</span>

						<div class="position_totalPrice-outsum">{{position.price_outSumDisplay}} р.</div>

						<div class="position_total-weight">{{position.weightSumDisplay}} кг</div>

						<div class="position_total-size"></div>
					</div>
					<img class="deletePosition" ng-click="positions.deletePosition(position)" src="/images/delete.png" >
				
				
				</div>
				<div class="questionForPosition" ng-show="position.questionForPositionVisible">
					<textarea class="form-control questionForPosition_text" ng-model="position.questionForPositionText"></textarea>
					<div class="questionForPosition_control-line">
						<button class="questionForPosition_to-comment-btn" ng-click="positions.addToCommentQuestionForPosition(position)" ng-show="position.questionForPositionStatus != 'none' && position.product != null && position.product.id != null">Добавить в комментарий</button>
						<input class="questionForPosition_status" type="checkbox" ng-show="position.questionForPositionStatus != 'none'" ng-model="position.questionForPositionStatusCheck" ng-change="positions.changeQuestionStatusForPosition(position)">
						<span class="questionForPosition_status-label" ng-show="position.questionForPositionStatus != 'none'">Активный вопрос</span>
						<button class="questionForPosition_save-btn" ng-click="positions.saveQuestionForPosition(position)">Сохранить</button>
						<button class="questionForPosition_delete-btn" ng-click="positions.deleteQuestionForPosition(position)" ng-show="position.questionForPositionStatus != 'none'">Удалить</button>
					</div>
				</div>
	 			<div id="liveProductSearch-{{position.id}}" class="liveProductSearch">
	 			</div>				

			</div>
	</div>
			
</div>






<!-- Карточка товара -->
<product-info-window windowshow="productInfoWindow.show" product="productInfoWindow.product" position="productInfoWindow.position" providers="providers"></product-info-window>


<!-- Для множественного добавления позиций -->
<div class="progress-box" ng-cloak ng-show="positions.multipleSearchQueue.visible">
	<div class="progress-box_text">
		Обработка <span> {{positions.multipleSearchQueue.currentIndex}}</span>/{{positions.multipleSearchQueue.lines.length}}
	</div>
	<div class="progress-box_finish" ng-show="positions.multipleSearchQueue.finish">
		<div class="progress-box_finish-text">Завершено!</div>
		<div class="btn btn-success" ng-click="positions.multipleSearchQueue.close()">Закрыть</div>
	</div>
</div>

<script type="text/ng-template" id="confirmTemplate.html">
    <p>{{showConfirmMessage}}</p>
	<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
	<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
</script>


</div>
</div>

<script type="text/javascript">
	var requisition_id = <?php echo  $requisition_id ?>;
    
    var itIsNewRequisition = <?php echo  $itIsNewRequisition?>;

    var reqProds =  <?php echo CJavaScript::encode($reqProds)?>;
    var providers =  <?php echo CJavaScript::encode($providers)?>;
    var initCustomer =  <?php echo CJavaScript::encode($customerInfo)?>;

    
</script>

