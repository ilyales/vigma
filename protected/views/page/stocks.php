<div ng-app="importStock">
<div ng-controller="importController" ng-init="init()">

<p class="stock-title">Остатки на складах</p>

<div class="stocks-table-wrap">
	<table class="table table-striped table-condensed ">
		<thead class="stocks-table_head">
			<tr>
				<td>Город</td>
				<td>Актуален на</td>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="stockCity in stockCities.items" >
				<td class="stocks-table_td-name">
					{{stockCity.name}} ({{stockCity.count}})
				</td>
				<td class="stocks-table_td-date">
					{{stockCity.date}}
				</td>
				<td class="stocks-table_td-edit">
					<span class="stocks-table_edit" ng-click="stockTemplate.edit(stockCity)">Редактировать</span>
				</td>
				<td class="stocks-table_td-clear">
					<span class="stocks-table_clear" ng-click="stockTemplate.clear(stockCity)">Очистить</span>
				</td>
				<td class="stocks-table_td-delete">
					<span class="stocks-table_delete" ng-click="stockTemplate.delete(stockCity)">Удалить</span>
				</td>
			</tr>				
		</tbody>
	</table>
	<div class="stocks-table_new" ng-click="stockTemplate.new()">
		<img class="stocks-table_new-img" src="/images/plus.png">
		Новый шаблон
	</div>
</div>
<div class="stock-template" ng-show="stockTemplate.visible">
	<div class="stock-template_title">Редактор шаблона складов</div>
	<div class="stock-template_line">
		<div class="stock-template_label">Город</div>
		<input type="text" class="form-control stock-template_value stock-template_value__long" ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.name}" ng-model="stockTemplate.items.name">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер в карточке товара</div>
		<input type="text" class="form-control stock-template_value "  ng-model="stockTemplate.items.priority">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер первой строки</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.startrownum}" ng-model="stockTemplate.items.startrownum">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки с Названием и ЧОД</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.chodnameclm}" ng-model="stockTemplate.items.chodnameclm">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки Основной кол-во</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.val1clm}" ng-model="stockTemplate.items.val1clm">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки Основной цена</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.price1clm}" ng-model="stockTemplate.items.price1clm">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки Покупка кол-во</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.val2clm}" ng-model="stockTemplate.items.val2clm">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки Покупка цена</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.price2clm}" ng-model="stockTemplate.items.price2clm">
	</div>
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки Четра кол-во</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.val3clm}" ng-model="stockTemplate.items.val3clm">
	</div>	
	<div class="stock-template_line">
		<div class="stock-template_label">Номер кол-ки Четра цена</div>
		<input type="text" class="form-control stock-template_value " ng-class="{'stock-template_value__error':!stockTemplate.itemsValid.price3clm}" ng-model="stockTemplate.items.price3clm">
	</div>	

	<button class="btn btn-success stock-template_saveBtn" ng-click="stockTemplate.save()">Сохранить</button>
	<button class="btn btn-warning stock-template_cancelBtn" ng-click="stockTemplate.cancel()">Омена</button>
</div>

<p class="stock-title">Импорт остатков</p>

<div class="import-line">
	<select class="form-control import-line_price-type" ng-change="stockCities.change()" ng-model="stockCities.value" >
		<option ng-repeat="stockCity in stockCities.items" ng-selected="stockCity.id==1" ng-value="stockCity.id">{{stockCity.name}}</option>
	</select>
	<div class="import-line_file-name" ng-bind="file.name"></div>
	<button class="btn btn-default import-line_loadfile-btn" ngf-select="file.upload($file)">Выбрать файл</button>
	<img src="images/loading.gif" class="import-line_loading" ng-show="file.loading">
</div>

<div class="import-line-labels">
	<div class="import-line_import-date-label">Актуален на:</div>
	<div class="import-line_smirnyaginSheetCount-label" ng-show="priceType==4">Количество листов:</div>
</div>

<div class="import-line">
	<input id="priceActualDate"  ng-model="import.date" type="text" class="form-control import-line_import-date">
	
	<button class="btn btn-success import-line_loadfile-btn" ng-disabled="!file.loaded" ng-click="import.start()">Начать импорт</button>
	<img src="images/loading.gif" class="import-line_loading" ng-show="import.wait">
</div>

<div class="result-types_wrap" ng-show="showResults">
	<table class="table table-striped result-types_table">
		<thead>
			<tr class="result-types_head-tr">
				<th class="result-types_td-name result-types_td-name__head">Результаты импорта</th>
				<th class="result-types_td-count">Кол-во</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Значения обновлены</b>
				</td>
				<td class="result-types_td-count">
					{{result.updateProds.fileRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="updatedProds.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Значения не поменялись</b>
				</td>
				<td class="result-types_td-count">
					{{result.notNeedUpdateProds.fileRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="notNeedUpdateProds.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Нет связи с товаром</b><br>
					Найдено <b>1 совпадение</b> по ЧОД (можно создать связи автоматически)
				</td>
				<td class="result-types_td-count">
					{{result.oneProds.fileRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="oneProds.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Нет связи с товаром</b><br>
					Найдено <b>несколько совпадений</b> по ЧОД (нужно создать связи вручную)
				</td>
				<td class="result-types_td-count">
					{{result.severalProds.fileRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="severalProds.show()">просмотр</span>
				</td>				
			</tr>	
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Нет связи с товаром</b><br>
					<b>Не найдено совпадений</b> по ЧОД (нужно создать связи вручную)
				</td>
				<td class="result-types_td-count">
					{{result.notFoundProds.fileRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="notFoundProds.show()">просмотр</span>
				</td>				
			</tr>
			<tr class="result-types_body-tr">
				<td class="result-types_td-name">
					<b>Исчезли из файла остатков</b><br>
					Товары, которые были в прошлой версии файла остатков, но не найдены в текущей (значения обнулены)
				</td>
				<td class="result-types_td-count">
					{{result.notInFile.fileRows.length}}
				</td>
				<td class="result-types_td-show">
					<span class="result-types_show" ng-click="notInFile.show()">просмотр</span>
				</td>				
			</tr>					
		</tbody>
	</table>
</div>

<div class="reslut-box" ng-show="updatedProds.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="updatedProds.close()">закрыть</div>
		<div class="result-box_title">Значения обнолвены ({{result.updateProds.fileRows.length}})</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
				<thead>
					<th width="10" >№</th>
					<th width="400" >ЧОД Название из файла остатков</th>
					<th width="400" >ориг. ЧОД Название из базы данных</th>
					
				</thead>
				<tbody>
					<tr ng-repeat="row in result.updateProds.fileRows">
						<td width="10">{{$index+1}}</td>
						<td width="400">{{row.chodname}}</td>
						<td width="400">{{row.product.chod_display}} {{row.product.original_name}}</td>
						<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row.product)"></td>
					</tr>
				</tbody>
		</table>
	</div>
</div>

<div class="reslut-box" ng-show="notNeedUpdateProds.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="notNeedUpdateProds.close()">закрыть</div>
		<div class="result-box_title">Значения не поменялись ({{result.notNeedUpdateProds.fileRows.length}})</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
				<thead>
					<th width="10" >№</th>
					<th width="400" >ЧОД Название</th>
					<th width="400" >ориг. ЧОД Название из базы данных</th>
				</thead>
				<tbody>
					<tr ng-repeat="row in result.notNeedUpdateProds.fileRows">
						<td width="10">{{$index+1}}</td>
						<td width="400">{{row.chodname}}</td>
						<td width="400">{{row.product.chod_display}} {{row.product.original_name}}</td>
						<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row.product)"></td>
					</tr>
				</tbody>
		</table>
	</div>
</div>

<div class="reslut-box" ng-show="oneProds.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="oneProds.close()">закрыть</div>
		<div class="result-box_title">
			<b>Нет связи с товаром</b>
			Найдено <b>1 совпадение</b> по ЧОД (можно создать связи автоматически) 
			({{result.oneProds.fileRows.length}})
		</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="10" >№</th>
				<th width="400" >ЧОД Название из файла остатков</th>
				<th width="400" >ориг. ЧОД Название из базы данных</th>
				<th></th>
			</thead>
			<tbody>
				<tr ng-repeat="row in result.oneProds.fileRows">
					<td width="10">{{$index+1}}</td>
					<td width="400">{{row.chodname}}</td>
					<td width="400">{{row.chod_display}} {{row.original_name}}</td>
					<td>
						<input type="checkbox" ng-model="row.needAdd" ng-show="!row.added">
						<span class="reslut-box_add-label"  ng-show="row.added"><img class="reslut-box_add-label-img" src="/images/success.png"></span>
					</td>
					<td>
						<span class="reslut-box_selectProduct" ng-click="liveSearch.showLiveSearch(row)">выбрать товар</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="reslut-box_bottom" ng-show="result.oneProds.fileRows.length>0">
		<button class="btn btn-default" ng-click="oneProds.multiAddToProduct.start()">Связать с выбранными товарами</button>
		<span class="reslut-box_bottom-ch-all" ng-click="oneProds.selectAll()">отметить все </span>
		<span class="reslut-box_bottom-unch-all" ng-click="oneProds.unSelectAll()">снять все</span>
	</div>
</div>

<div class="reslut-box" ng-show="severalProds.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="severalProds.close()">закрыть</div>
		<div class="result-box_title">
			<b>Нет связи с товаром</b>
			Найдено <b>несколько совпадений</b> по ЧОД (нужно создать связи вручную) 
			({{result.severalProds.fileRows.length}})
		</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="10" >№</th>
				<th width="400" >ЧОД Название из файла остатков</th>
			</thead>
			<tbody>
				<tr ng-repeat="row in result.severalProds.fileRows">
					<td width="10">{{$index+1}}</td>
					<td width="400">{{row.chodname}}</td>
					<td>
						<span class="reslut-box_add-label"  ng-show="row.added"><img class="reslut-box_add-label-img" src="/images/success.png"></span>
					</td>
					<td>
						<span class="reslut-box_selectProduct" ng-click="liveSearch.showLiveSearch(row)">выбрать товар</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="reslut-box" ng-show="notFoundProds.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="notFoundProds.close()">закрыть</div>
		<div class="result-box_title">
			<b>Нет связи с товаром</b>
			<b>Не найдено совпадений</b> по ЧОД (нужно создать связи вручную):
			(<b>{{result.notFoundProds.fileRows.length}}</b>)
		</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="10" >№</th>
				<th width="400" >ЧОД Название из файла остатков</th>
			</thead>
			<tbody>
				<tr ng-repeat="row in result.notFoundProds.fileRows">
					<td width="10">{{$index+1}}</td>
					<td width="400">{{row.chodname}}</td>
					<td>
						<span class="reslut-box_add-label"  ng-show="row.added"><img class="reslut-box_add-label-img" src="/images/success.png"></span>
					</td>
					<td>
						<span class="reslut-box_selectProduct" ng-click="liveSearch.showLiveSearch(row)">выбрать товар</span>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="reslut-box" ng-show="notInFile.visible">
	<div class="reslut-box_bg"></div>
	<div class="reslut-box_head">
		<div class="result-box_close" ng-click="notInFile.close()">закрыть</div>
		<div class="result-box_title">Товары, которые были в прошлой версии файла остатков, но не найдены в текущей (значения обнулены)  ({{result.notInFile.fileRows.length}})</div>
	</div>
	<div class="reslut-box_content">		
		<table class="table table-striped result-box-table">
			<thead>
				<th width="10" >№</th>
				<th width="400" >ориг. ЧОД Название из базы данных</th>
			</thead>
			<tbody>
				<tr ng-repeat="row in result.notInFile.fileRows">
					<td width="10">{{$index+1}}</td>
					<td width="400">{{row.product.chod_display}} {{row.product.original_name}}</td>	
					<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row.product)"></td>		
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="progress-box" ng-show="progressBox.visible">
	<div class="progress-box_bg"></div>
	<div class="progress-box_text">
		Обработка <span> {{progressBox.currentPosition}}</span>/{{progressBox.max}}
	</div>

	<div class="progress-box_finish" ng-show="progressBox.finish">
		<div class="progress-box_finish-text">Завершено!</div>
		<div class="btn btn-success" ng-click="progressBox.close()">Закрыть</div>
		
		<div class="progress-box_errors-wrap">
			<div class="progress-box_errors" ng-show="progressBox.errors.length!=0">
				<div class="progress-box_errors-title">Ошибки:</div>
				<div class="progress-box_errors-text">
					<p ng-repeat="text in progressBox.errors track by $index">{{text}}</p>
				</div>
			</div>
			<div class="progress-box_errors" ng-show="progressBox.notices.length!=0">
				<div class="progress-box_errors-title">Предупреждения:</div>
				<div class="progress-box_errors-text">
					<p ng-repeat="text in progressBox.notices track by $index">{{text}}</p>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Карточка товара -->
<product-info-window windowshow="productInfoWindow.show"  product="productInfoWindow.product" providers="productInfoWindow.providers"></product-info-window>

<div id="liveSearch" class="liveSearch" ng-show="liveSearch.visible">
	<img class="liveSearch_close" src="images/close.png" ng-click="liveSearch.closeLiveSearch()">

	<div></div>
	<div class="row allproducts-search" >
		<div class="col-xs-3">
			<label class="liveSearch_searchLabel">Наименование:</label>
			<input type="text" class="liveSearch_searchInp" ng-model="liveSearch.filter.name" />
			<img class="liveSearch_searchClear" src="/images/close-gray.png" ng-click="liveSearch.clearFilter('name')">			
		</div>
		<div class="col-xs-3">
			<label class="liveSearch_searchLabel liveSearch_searchLabel__chod">ЧОД:</label>
			<img class="liveSearch_searchClear" src="/images/close-gray.png" ng-click="liveSearch.clearFilter('chod')">	
			<input type="text" class="liveSearch_searchInp" ng-model="liveSearch.filter.chod" />
			
		</div>

		<div class="col-xs-6 liveSearch_providerInfo">
			<div>
				Название из файла-остатков:<span class="liveSearch_fileChodName">{{liveSearch.tableRow.chodname}}</span>
				
			</div>
			
		</div>
	</div>

	<div  tasty-table  bind-resource-callback="liveSearch.getResource"  bind-init="liveSearch.initTasty" bind-filters="liveSearch.filter" >
			<table class="table table-striped table-condensed allproducts-table">
				<thead tasty-thead></thead>
				<tbody>
					<tr ng-repeat="row in rows" >
						<td >
							{{ row.product.original_name }}
							<coincident-input-requsets items="row.product.inputRequests" field="name" value="filter.name" />
						</td>
						<td >
							{{ row.product.chod_display }}
							<coincident-input-requsets items="row.product.inputRequests" field="chod" value="filter.chod" />
						</td>
						<td >
							{{ row.product.product_num }}
						</td>
						<td ><img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row.product)"></td>
						<td><span class="liveSearch_selectProduct" ng-click="liveSearch.selectProduct(row.product)">Привязать остаток к товару</span></td>
					</tr>
				</tbody>
			</table>
			<div tasty-pagination bind-items-per-page="liveSearch.itemsPerPage" bind-list-items-per-page="liveSearch.listItemsPerPage"></div>
	</div>
</div>
<div class="liveSearch-bg" ng-show="liveSearch.visible" ng-click="liveSearch.closeLiveSearch()"></div>

</div>
</div>

<script type="text/javascript">
	var stockCities = <?php echo CJavaScript::encode($stockCities)?>;
	var providers = <?php echo CJavaScript::encode($providers)?>;
</script>
