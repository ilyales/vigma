<div ng-app="allCustomers">
<div ng-controller="allCustomersController" ng-init="init()">
<h1 class="customers_title">Контрагенты</h1>

<div class="customers_list" >
	<div class="customers_list-top" ng-class="{'customers_list-top__fixed':customersList.fixedTop}">
		<div class="customers_filters">
			<input class="customers_filters-name" type="text" placeholder="Поиск по названию" ng-model="customersList.filter.name">
			<button class="btn btn-default customers_newBtn" ng-click="customerEdit.show()">Добавить контрагента</button>
			<button class="btn btn-default customers_showStatusEditBtn" ng-click="statusesEdit.show()">Редактор статусов</button>
		</div>
		<div class="customers_list-head">
			<div class="customer_list-name customers_list-head-item" ng-click="customersList.changeOrder('name')">Имя</div>
			<div class="customer_list-status customers_list-head-item" ng-click="customersList.changeOrder('-status')">Статус</div>
			<div class="customer_list-reqCount customers_list-head-item" ng-click="customersList.changeOrder('-reqNum')">Кол-во заявок</div>
			<!-- <div class="customer_list-showFullInfo customers_list-head-item"></div> -->
		</div>
	</div>
	<div class="customers_list-body" ng-class="{'customers_list-body__fixed':customersList.fixedTop}">
		<div class="customers_list-row" 
			 ng-repeat="customer in customers | filter:customersList.filter | orderBy:customersList.order.value" 
			 ng-class="{'customers_list-row_even':$index % 2 == 0,'customers_list-row_selected':customer.selected}"
			 ng-click="customersList.select(customer)"
		>
			<div class="customer_list-name" >
				<span class="customer_list-name-val" ng-click="customerEdit.show(customer)">{{customer.name}}</span>
			</div>
			<div class="customer_list-status">{{customer.status}}</div>
			<div class="customer_list-reqCount">{{customer.reqNum}}</div>
			<div class="customer_list-delete"><img class="customer_list-delete-img" src="/images/delete.png" ng-click="customersList.delete(customer)"></div> 
		</div>
	</div>	
</div>

<customeredit visible='customerEdit.visible' customer="customerEdit.customer" new-cust-save-cback="customersList.add" statuses="statuses"></customeredit>

<div class="statusesEdit-bg" ng-show="statusesEdit.visible"></div>
<div class="statusesEdit" ng-show="statusesEdit.visible">
	<div class="statusesEdit_list">
		<div class="statusesEdit_list-row" ng-repeat="status in statuses" ng-class="{'statusesEdit_list-row__even':$index % 2 !=0}">
			<div class="statusesEdit_list-name" >
				<span class="statusesEdit_list-name-val" editable-text="status.name" onaftersave="statusesEdit.updateStatus(status)">{{status.name}}</span>
			</div>
			<div class="statusesEdit_list-delete">
				<img class="statusesEdit_list-delete-img" src="/images/delete.png" ng-click="statusesEdit.deleteStatus(status)">
			</div> 
		</div>
	</div>
	<div class="statusesEdit_btns">
		<button class="btn btn-default statusesEdit_closeBtn" ng-click="statusesEdit.close()">Готово</button>
		<button class="btn btn-success statusesEdit_addStatusBtn" ng-click="statusesEdit.addStatus()">Добавить статус</button>
	</div>
	
</div>

<script type="text/ng-template" id="confirmTemplate.html">
    <p>{{showConfirmMessage}}</p>
	<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
	<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
</script>


</div>
</div>

<script type="text/javascript">
	
	var customers = <?php echo CJavaScript::encode($customers)?>;
    var statuses = <?php echo CJavaScript::encode($statuses)?>;
</script>
