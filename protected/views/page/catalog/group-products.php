<?php $this->renderPartial("catalog/common/menu",array('tab'=>$tab)); ?>

<div ng-app="groups" class="groups">
	<div ng-controller="groupProductsController" ng-init="init()" ng-cloak>
		<p class="groups-title">Группы</p>

		<div class="row groups-prod-search" >
			<div class="col-xs-3">
					<label class="groups-prod_searchLabel">Наименование:</label>
					<input type="text" class="groups-prod_searchInp" ng-model="products.filter.name" />
					<img class="groups-prod_searchClear" src="/images/close-gray.png" ng-click="products.clearFilter('name')">			
				</div>
				<div class="col-xs-3">
					<label class="groups-prod_searchLabel">ЧОД:</label>
					<input type="text" class="groups-prod_searchInp" ng-model="products.filter.chod" />
					<img class="groups-prod_searchClear" src="/images/close-gray.png" ng-click="products.clearFilter('chod')">	
				</div>
				<div class="col-xs-3">
					<label class="groups-prod_searchLabel">Группа:</label>
					<select class="groups-prod_select"  
							ng-model="groups.selected" 
							ng-options="group.group_num for group in groups.items"
							ng-change="groups.changed()"
					></select>
				</div>
				<div class="col-xs-3">
					<img src="/images/loading.gif" class="groups-prod_loading" ng-show="products.dataLoading">
				</div>
		</div>

		<div  tasty-table
			  bind-resource-callback="products.getResource" 
			  bind-init="products.initTasty" 
			  bind-filters="products.filter"
			  bind-reload="products.reloadCallback"
		>
			<table class="table table-striped table-condensed groups-prod-table">
				<thead tasty-thead></thead>
				<tbody>
					<tr ng-repeat="row in rows track by $index" ng-class="{'groups-prod-table_active-row':row.active!=null && row.active==true}" ng-click="products.setProductRowActive(row)">
						<td width="350">
							{{ row.original_name }}
							</td>
						<td width="350" >
							{{ row.chod_display }}
						</td>
						<td width="100" >
							<span ng-show="row.img_exists==1">есть</span>
							<span ng-show="row.img_exists==0">нет</span>							
						</td>
						<td width="50" >
							{{ row.rating }}
						</td>
						<td width="200" >
							{{ row.groups_str }}
						</td>
						<td width="200" >
							<span class="groups-prod-table_edit" ng-click="products.showGroupEditor(row,$index)">редактировать</span>
						</td>
						<td><img class="showProductInfoWin" src="/images/list.png" ng-click="productInfoWindow.showProductsInfoWin(row)"></td>
					</tr>
				</tbody>
			</table>
			<div tasty-pagination bind-items-per-page="products.itemsPerPage" bind-list-items-per-page="products.listItemsPerPage"></div>
			<div class="groups-prod_bottom-loading">
				<img src="/images/loading.gif" class="groups-prod_bottom-loading-img" ng-show="products.dataLoading">
			</div>
		</div>

		<!-- Редактор связей -->
		<group-prod-editor class="gp-editor" 
						   product="products.groupEditorProduct" 
						   update-product-groups="products.updateProductGroups(groups_ids,groups_str)" 
						   all-groups="groups.items"
						   top-offset="products.groupEditorTopOffset"						   
						   >						   
		</group-prod-editor>
		<!-- Карточка товара -->
		<product-info-window windowshow="productInfoWindow.show"  product="productInfoWindow.product" providers="providers"></product-info-window>
	
	</div> <!-- ng-controller -->
</div> <!-- ng-app -->

<script type="text/ng-template" id="confirmTemplate.html">
    <p>{{showConfirmMessage}}</p>
	<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
	<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
</script>

<script type="text/javascript">
	var providers = <?php echo CJavaScript::encode($providers)?>;
	var catalogcats = <?php echo CJavaScript::encode($catalogcats)?>;
	var groups = <?php echo CJavaScript::encode($groups)?>;
</script>