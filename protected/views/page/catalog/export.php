<?php $this->renderPartial("catalog/common/menu",array('tab'=>$tab)); ?>

<div ng-app="groups">
	<div ng-controller="exportController" ng-init="init()">
		<p class="groups-title">Экспорт</p>
		<a href="/index.php?r=productsApi/exportForCatalog" class="btn btn-default exportCatalog">Полный экспорт</a>
		<waitBox></waitBox>
	</div> <!-- ng-controller -->	
</div> <!-- ng-app -->

<script type="text/ng-template" id="confirmTemplate.html">
    <p>{{showConfirmMessage}}</p>
	<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
	<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
</script>

<script type="text/javascript">
	var providers = <?php echo CJavaScript::encode($providers)?>;
	var catalogcats = <?php echo CJavaScript::encode($catalogcats)?>;
	var groups = <?php echo CJavaScript::encode($groups)?>;
</script>