<div ng-app="AllRequisitions" ng-controller="AllRequisitionsController as vm">
	<h1 class="allrequisitions_title">
		Все заявки
	</h1>
	<requisitions-list title="Подтвержденные заявки" requisitions="vm.confirmedRequisitions" status="confirmed" total-items="vm.confirmedRequisitionsTotalCount"></requisitions-list>
	<requisitions-list title="Ожидающие подтверждения заявки" requisitions="vm.waitForConfirmRequisitions" status="first" total-items="vm.waitForConfirmRequisitionsTotalCount"></requisitions-list>

	<script type="text/ng-template" id="confirmTemplate.html">
	    <p>{{showConfirmMessage}}</p>
		<input type="button" value="Да" class="btn btn-info" ng-click="confirm()"/>
		<input type="button" value="Отмена" class="btn btn-default" ng-click="closeThisDialog(0)"/>
	</script>

</div>

<script type="text/javascript">
	var confirmedRequisitions = <?php echo CJavaScript::encode($confirmedRequisitionsInfo) ?>;
	var confirmedRequisitionsTotalCount = <?php echo CJavaScript::encode($confirmedRequisitionsTotalCount) ?>;
	var waitForConfirmRequisitions = <?php echo CJavaScript::encode($waitForConfirmRequisitionsInfo) ?>;
	var waitForConfirmRequisitionsTotalCount = <?php echo CJavaScript::encode($waitForConfirmRequisitionsTotalCount) ?>;
</script>