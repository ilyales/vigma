<?php
class GroupsApiController extends Controller
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

    public function actionImportGroupsAjax()
    {
        $data = json_decode(file_get_contents('php://input'));  

        $response = array('status'=>true);

        try {
            $filePath = UploadFile::getPathByName($data->fileName);
        
            $result = GroupProdsManager::importGroupsFromFile($filePath);

            $response['result'] = $result;
            CatalogcatsManager::updateProductsCatalogcatIds();
        }
        catch (Exception $e)
        {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }       
        

        echo json_encode($response);  
    }

    public function actionAddGroupToCatalogcatAjax()
    {
        $data = json_decode(file_get_contents('php://input')); 

        $response = array('status'=>true);

        try {
            CatalogcatsManager::addGroupToCatalogcat($data->catalogcatId,$data->groupId);
            CatalogcatsManager::updateProductsCatalogcatIds();
        }
        catch (Exception $e)
        {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }       
        

        echo json_encode($response);
    }

    public function actionRemoveGroupFromCatalogcatAjax()
    {
        $data = json_decode(file_get_contents('php://input')); 
        $response = array('status'=>true);

        try {
            CatalogcatsManager::removeGroupFromCatalogcat($data->catalogcatId,$data->groupId);
            CatalogcatsManager::updateProductsCatalogcatIds();
        }
        catch (Exception $e)
        {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }       
        

        echo json_encode($response);
    }

    public function actionGetPtoductsByCatalogcatAjax()
    {
        $data = json_decode(file_get_contents('php://input')); 

        $response = array('status'=>true);

        try {
            $products = Products::getProductsByCatalogcat($data->catalogcatId);
            $response['products'] = $products;
        }
        catch (Exception $e)
        {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        } 

        echo json_encode($response);
    }

    // public function actionGetGroupsByProductIdAjax() {
    //     $data = json_decode(file_get_contents('php://input'));
    //     $response = array();
    //     $response['status'] = true;
    //     try {
    //         $products = GroupProdsManager::deleteGroupProd($data->groupId,$data->productId);
    //     }
    //     catch (Exception $e)
    //     {
    //         $response['status'] = false;
    //         $response['message'] = $e->getMessage();
    //     }   
    //     echo json_encode($response);
    // }

    /**
     *  Добавить новую связь группы и товара
     * 
     */ 
    public  function actionNewGroupProd()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array();
        $response['status'] = true;

        try
        {
            $res = GroupProdsManager::newGroupProds($data->groupId,$data->productId,$data->chod); 
            //если связь длбавляется вручную, то надо обновить таблицу catalogcat_prods
            if ($data->chod==null) {
                CatalogcatsManager::updateProductsCatalogcatIds();
            }
            if ($res==false)
            {
                $response['status'] = false;
                $response['message'] = "к товару уже привязан группа ".$data->group_num;
            }
        }
        catch (Exception $e)
        {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        }

        echo json_encode($response);
    }

    /**
     * Удлить запись в group_prods по group_id и product_id
     * @post int $group_id
     * @post int $product_id
     */ 
    public function actionUnbindGroupFromProductAjax()
    {
        $data = json_decode(file_get_contents('php://input'));
        $response = array();
        $response['status'] = true;

        try {
            $products = GroupProdsManager::deleteGroupProd($data->groupId,$data->productId);
            CatalogcatsManager::updateProductsCatalogcatIds();
        }
        catch (Exception $e)
        {
            $response['status'] = false;
            $response['message'] = $e->getMessage();
        } 

        echo json_encode($response);
    }


    /**
     * Выборка товаров относящихся к группе
     * @post $groupId
     */     
    public function actionGetPtoductsByGroupAjax()
    {
        $headers = array();
        array_push($headers, array("key"=>"name","name"=>"Наименование"));
        array_push($headers, array("key"=>"chod","name"=>"ЧОД"));
        array_push($headers, array("key"=>"chod","name"=>"Изображение"));
        array_push($headers, array("key"=>"chod","name"=>"Рейтинг"));
        array_push($headers, array("key"=>"groups","name"=>"Группы"));

        $result = array();
        $result['header'] = $headers;
        
        $page = intval($_GET['page']);
        $count = intval($_GET['count']);
        $offset = $page*$count-$count;

        $filters=array();
        $filters['name'] = $_GET['name'];
        $filters['chod'] = preg_replace('/[^a-zа-яё\d]/ui','',$_GET['chod']);
        $filters['groupId'] = $_GET['groupId'];
        
        $res = GroupProdsManager::getProductsByGroup($filters,$offset,$count);
        
        $result['rows'] = $res['products'];
        $resultsCount = $res['resultsCount'];

        $pagination = array();
        $pagination['count'] = $count;
        $pagination['page'] = $page;
        $pagination['pages'] = ($resultsCount-$resultsCount%$count)/$count;
        $pagination['size'] = intval($resultsCount);

        $result['pagination'] = $pagination;
        $result['sort-by'] = $_GET['sort-by'];
        $result['sort-order'] = $_GET['sort-order'];
        $result['products-count'] = count($products);

        echo json_encode($result);
    }
}