<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $original_name
 * @property string $original_chod
 * @property string $chod_display
 * @property string $product_num
 * @property string $comment
 * @property string $tovar_type
 * @property string $other_info
 * @property integer $defaultprovider_id
 * @property double $price_out
 * @property string $size
 * @property double $weight
 * @property integer $parts_id
 * @property integer $partof_id
 * @property string $use_deafult
 *
 * The followings are the available model relations:
 * @property InputRequests[] $inputRequests
 * @property ProvProds[] $provProds
 */
class VitrinaProducts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('original_name, original_chod, chod_display', 'required'),
			array('defaultprovider_id, parts_id, partof_id', 'numerical', 'integerOnly'=>true),
			array('price_out, weight', 'numerical'),
			array('original_name, original_chod, product_num, comment, size', 'length', 'max'=>256),
			array('chod_display', 'length', 'max'=>255),
			array('tovar_type', 'length', 'max'=>20),
			array('other_info, use_deafult', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, original_name, original_chod, chod_display, product_num, comment, tovar_type, other_info, defaultprovider_id, price_out, size, weight, parts_id, partof_id, use_deafult', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'inputRequests' => array(self::HAS_MANY, 'InputRequests', 'product_id'),
			'provProds' => array(self::HAS_MANY, 'ProvProds', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'original_name' => 'Original Name',
			'original_chod' => 'Original Chod',
			'chod_display' => 'Chod Display',
			'product_num' => 'Product Num',
			'comment' => 'Comment',
			'tovar_type' => 'Tovar Type',
			'other_info' => 'Other Info',
			'defaultprovider_id' => 'Defaultprovider',
			'price_out' => 'Price Out',
			'size' => 'Size',
			'weight' => 'Weight',
			'parts_id' => 'Parts',
			'partof_id' => 'Partof',
			'use_deafult' => 'Use Deafult',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('original_chod',$this->original_chod,true);
		$criteria->compare('chod_display',$this->chod_display,true);
		$criteria->compare('product_num',$this->product_num,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('tovar_type',$this->tovar_type,true);
		$criteria->compare('other_info',$this->other_info,true);
		$criteria->compare('defaultprovider_id',$this->defaultprovider_id);
		$criteria->compare('price_out',$this->price_out);
		$criteria->compare('size',$this->size,true);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('parts_id',$this->parts_id);
		$criteria->compare('partof_id',$this->partof_id);
		$criteria->compare('use_deafult',$this->use_deafult,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->vitrinaDb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VitrinaProducts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
