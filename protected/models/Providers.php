<?php

/**
 * This is the model class for table "providers".
 *
 * The followings are the available columns in table 'providers':
 * @property integer $id
 * @property string $name
 * @property integer $hidden
 * @property string $price_template
 * @property string $price_date
 * @property string $default_coef
 * @property string $min_coef
 * @property string $catalog_coef
 * @property integer $chtzprice_rull
 * @property integer $sort_num
 * @property string $info
 * @property string $import_comment
 *
 * The followings are the available model relations:
 * @property ProvProds[] $provProds
 * @property ReqProds[] $reqProds
 */
class Providers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'providers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('hidden, chtzprice_rull, sort_num', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>256),
			array('default_coef, min_coef, catalog_coef', 'length', 'max'=>4),
			array('price_template, price_date, info, import_comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, hidden, price_template, price_date, default_coef, min_coef, catalog_coef, chtzprice_rull, sort_num, info, import_comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'provProds' => array(self::HAS_MANY, 'ProvProds', 'provider_id'),
			'reqProds' => array(self::HAS_MANY, 'ReqProds', 'provider_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'hidden' => 'Hidden',
			'price_template' => 'Price Template',
			'price_date' => 'Price Date',
			'default_coef' => 'Default Coef',
			'min_coef' => 'Min Coef',
			'catalog_coef' => 'Catalog Coef',
			'chtzprice_rull' => 'Chtzprice Rull',
			'sort_num' => 'Sort Num',
			'info' => 'Info',
			'import_comment' => 'Import Comment',
		);
	}

	public function getInfoArray($fullInfo=false)
	{

		$providerInfo = array();
		$providerInfo['id'] = $this->id;
		$providerInfo['name'] = $this->name;		
		
		$providerInfo['default_coef'] = $this->default_coef;
		$providerInfo['min_coef'] = $this->min_coef;
		$providerInfo['catalog_coef'] = $this->catalog_coef;
		$providerInfo['chtzprice_rull'] = $this->chtzprice_rull;
		$providerInfo['sort_num'] = $this->sort_num;
		$providerInfo['import_comment'] = $this->import_comment;
		if ($this->price_date!=null) $providerInfo['price_date'] = date('d.m.Y',strtotime($this->price_date));
		else $providerInfo['price_date'] = null;
		
		if ($fullInfo==true)
		{
			$info = json_decode($this->info,true);
			
			if (is_array($info))
			{
				$infoKeys = array_keys($info);
				foreach ($infoKeys as $key)
				{
					$providerInfo[$key] = $info[$key];
				}
			}

			$providerInfo['price_template'] = json_decode($this->price_template,true);
		}

		return $providerInfo;
	}

	public function getAllPovidersInfo($fullInfo=false)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "hidden=0";
		$criteria->order = 'sort_num';

		$providers = $this->findAll($criteria);
		$providersInfo = array();
		foreach ($providers as $provider)
		{
			array_push($providersInfo, $provider->getInfoArray($fullInfo));
		}
		return $providersInfo;
	}

	public function updateProvider($providerInfo)
	{
		$provider = $this->findByPk($providerInfo->id);
		if ($provider==null) return false;

		try
		{
			$provider->name = $providerInfo->name;
			$provider->default_coef = $providerInfo->default_coef;
			$provider->min_coef = $providerInfo->min_coef;
			$provider->catalog_coef = $providerInfo->catalog_coef;

			$info = array();
			$info['full_name'] = $providerInfo->full_name;
			$info['contact_person'] = $providerInfo->contact_person;
			$info['phone'] = $providerInfo->phone;
			$info['email'] = $providerInfo->email;
			$info['address'] = $providerInfo->address;
			$info['legal_address'] = $providerInfo->legal_address;
			$info['mail_address'] = $providerInfo->mail_address;
			$info['bank_name'] = $providerInfo->bank_name;
			$info['payment_number'] = $providerInfo->payment_number;
			$info['bik'] = $providerInfo->bik;
			$info['corr_number'] = $providerInfo->corr_number;
			$info['bank_inn'] = $providerInfo->bank_inn;
			$info['director_name'] = $providerInfo->director_name;
			$info['director_job'] = $providerInfo->director_job;
			$info['confirm_doc'] = $providerInfo->confirm_doc;

			$provider->info = json_encode($info);
			$res = $provider->update();
			return $res;
		}
		
		catch(Exception $e)
		{
			return false;
		}		
	}

	public static function addProvider($providerInfo)
	{
		$provider = new Providers;
		
		
		$provider->name = $providerInfo->name;
		$provider->hidden = 0;
		$provider->sort_num = $providerInfo->sort_num;
		$provider->default_coef = $providerInfo->default_coef;
		$provider->min_coef = $providerInfo->min_coef;
		$provider->catalog_coef = $providerInfo->catalog_coef;

		$info = array();
		$info['full_name'] = $providerInfo->full_name;
		$info['contact_person'] = $providerInfo->contact_person;
		$info['phone'] = $providerInfo->phone;
		$info['email'] = $providerInfo->email;
		$info['address'] = $providerInfo->address;
		$info['legal_address'] = $providerInfo->legal_address;
		$info['mail_address'] = $providerInfo->mail_address;
		$info['bank_name'] = $providerInfo->bank_name;
		$info['payment_number'] = $providerInfo->payment_number;
		$info['bik'] = $providerInfo->bik;
		$info['corr_number'] = $providerInfo->corr_number;
		$info['bank_inn'] = $providerInfo->bank_inn;
		$info['director_name'] = $providerInfo->director_name;
		$info['director_job'] = $providerInfo->director_job;
		$info['confirm_doc'] = $providerInfo->confirm_doc;

		$provider->info = json_encode($info);
		$res = $provider->save();
		return $res;
				
	}



	public static function saveTemplate($provider_id,$template)
	{
		switch ($provider_id) {
			//чтз
		    case 1:
		        $template->type = "special";
		        break;
		    //смирнягин
		    case 30:
		        $template->type = "special";
		        break;
		    default:
		       $template->type = "custom";
		}
    

		
		$json_template = json_encode($template);
		
		try
		{
			$provider = Providers::model()->findByPk($provider_id);
			$provider->price_template = $json_template;
			$res = $provider->update();
			if ($res==false) return false;
		}		
		catch(Exception $e)
		{
			return false;
		}
		
		return true;
	}

	public static function changeProviderOrder($order)
	{
		$sql = "UPDATE providers
				SET sort_num = CASE id ";

		$idsList = "";
		for ($i=0;$i<count($order);$i++)
		{
			$sql = $sql."WHEN ".$order[$i]->id." THEN ".$i." ";
			$idsList = $idsList.$order[$i]->id;
			if ($i<count($order)-1) $idsList = $idsList.",";
		}
		$sql = $sql."END WHERE id IN (".$idsList.");";

		try
		{
			$connection=Yii::app()->db;
			$command=$connection->createCommand($sql);
			$updatedProvidersCount = $command->execute();
		}		
		catch(Exception $e)
		{
			return false;
		}	

		return true;
	}

	public static function importPrice($provider_id,$fileName,$priceImportCoef,$priceActualDate)
	{
		$provider = Providers::model()->findByPk($provider_id);

		if ($provider->price_template==null) return false;
		$price_template = json_decode($provider->price_template);

		$import = new Import;
		//ввиду не типичности формата некотрых прайсов, для ихх импорта сделаны отдельные методы
		if ($price_template->type=="special")
		{	
			//чтз
			if ($provider_id==1) 
			{
				$result = $import->updateChtzPrice($price_template,$fileName,$priceImportCoef,$priceActualDate);
			}

			//смирнягин
			if ($provider_id==30) 
			{
				$sheetCount = $price_template->sheetCount;
				$result = $import->updateSmirnyaginPrice($fileName,$priceImportCoef,$priceActualDate,$sheetCount);
			}
		}

		if ($price_template->type=="custom")
		{
			$updatePriceChtz = $provider->chtzprice_rull;// если установлен флаг chtzprice_rull, то при импорте
														 // надо будет обновить поле price_chtz в таблице product, занеся туда цену из прайса
			$result = $import->updateCustomPrice($price_template,$provider_id,$fileName,$priceImportCoef,$priceActualDate,$updatePriceChtz);
		}

		if ($result!=null)
		{
			$provider = Providers::model()->findByPk($provider_id);
			$provider->price_date = date('Y-m-d',strtotime($priceActualDate));//date('Y-m-d'); 
			$res=$provider->update();
		}

		return $result;
	}

	public static function updateImportComment($provider_id,$import_comment)
	{
		$provider = Providers::model()->findByPk($provider_id);
		if ($provider == null) return false;

		$provider->import_comment = $import_comment;
		$provider->update();

		try {
			$provider->update();
		}
		catch (Exception $e)
		{
			return false;
		}

		return true;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('hidden',$this->hidden);
		$criteria->compare('price_template',$this->price_template,true);
		$criteria->compare('price_date',$this->price_date,true);
		$criteria->compare('default_coef',$this->default_coef,true);
		$criteria->compare('min_coef',$this->min_coef,true);
		$criteria->compare('catalog_coef',$this->catalog_coef,true);
		$criteria->compare('chtzprice_rull',$this->chtzprice_rull);
		$criteria->compare('sort_num',$this->sort_num);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('import_comment',$this->import_comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Providers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
