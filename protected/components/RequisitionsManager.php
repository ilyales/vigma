<?php
/**
* Методы для работы с заявками
*/
class RequisitionsManager
{
    const CART_STATUS_CHECKOUT = "checkout";
    const CART_STATUS_EXPORTED = "exported";

    /**
     * Получить спсико заявоу с выбранным статусом
     * @param string $status first | confirmed
     * @param int $count
     * @param int $offset     * 
     */ 
	public static function getRequisitions($status,$count=null,$offset=null) {
        if ($status!="first" && $status!="confirmed") {
            throw new Exception('Not valid status. Must be first|confirmed');
        }

		$criteria = new CDbCriteria();
        $criteria->compare('status',$status); 
        $criteria->order = 'modified DESC';

        if ($count!=null) {
        	$criteria->limit = $count;
        	
        	if ($offset!=null) {
        		$criteria->offset = $offset;
        	}
        }        

        $requisitions = Requisitions::model()->findAll($criteria);


        
        $requisitionsInfo = array();
        foreach ($requisitions as $requisition)
        {

            $requisitionInfo = array();
            $requisitionInfo['id'] = $requisition->id;
            $requisitionInfo['num'] = $requisition->num;
            $requisitionInfo['created'] = self::mysqTimeToRusTime($requisition->created);
            $requisitionInfo['modified'] = self::mysqTimeToRusTime($requisition->modified);
            $requisitionInfo['new'] = $requisition->new;
            
            $customer = Customers::model()->findByPk($requisition->customer_id);
            if ($customer!=null) $requisitionInfo['customerName'] = $customer->name;

            $criteria = new CDbCriteria();
            
            $criteria->condition = "requisition_id = $requisition->id";

            if ($status=="confirmed") {
                $criteria->addCondition("list_type = 'confirm'");
            }

            $products = ReqProds::model()->findAll($criteria);
            $requisitionInfo['productsCount'] = count($products);
            $requisitionInfo['url'] = Yii::app()->createUrl('page/requisition',array('id'=>$requisition->id));

            $requisitionsInfo[] = $requisitionInfo;
                        
        }

        return $requisitionsInfo;
	}


    /** 
     * Преобразовать mysql вреямя (datetime)  в русский формат
     * @param string  $mysqlTime
     * @return string $rusTime
     */
    protected function mysqTimeToRusTime($mysqlTime){
        $months = array( 1 => 'Янвь' , 'Фев' , 'Мар' , 'Апр' , 'Май' , 'Июн' , 'Июл' , 'Авг' , 'Сен' , 'Окт' , 'Ноя' , 'Дек' );
        $datetime = strtotime($mysqlTime);
        return date("d ".$months[date('n',$datetime)]." Y H:i",$datetime);
    }

    /**
     * Получить количество заявок с выбранным статусом
     * @param string $status first | confirmed
     */ 
    public static function getRequisitionsCount($status) {
        if ($status!="first" && $status!="confirmed") {
            throw new Exception('Not valid status. Must be first|confirmed');
        }

        $criteria = new CDbCriteria();
        $criteria->compare('status',$status); 
        $criteria->order = 'modified DESC';
        $count = Requisitions::model()->count($criteria);
        return $count;
    }

    /**
     * Удалить заявку со всеми позициями
     * @param int $id
     */ 
    public static function deleteRequisition($id) {
        $criteria = new CDbCriteria;
        $criteria->compare('requisition_id',$id);
        
        // обновить значения счетчика rating для всех товаров в заявке
        $reqProds = ReqProds::model()->findAll($criteria);

        foreach ($reqProds as $reqProd)
        {
            if ($reqProd->product_id!=null)
            {
                Products::model()->decreaseRating($reqProd->product_id);
            }
        }

        ReqProds::model()->deleteAll($criteria);

        Requisitions::model()->deleteByPk($id);
    }

    /**
     * Получить последний (максимальный) номер заявки
     * @return int $lastNum
     */
    public static function getLastNumber() {
        $sql=  "SELECT MAX(num) FROM requisitions";
        $connection=Yii::app()->db;
        $command=$connection->createCommand($sql);  
        $maxNum=$command->queryScalar();
        return $maxNum;
    }

    /**
     * Получить номер заявки по её id
     * @param int $id - id заявки
     * @return int $num
     */
    public static function getNum($id) {
        $criteria = new CDbCriteria;
        $criteria->select = "num";
        $criteria->compare('id',$id);
        $requisition = Requisitions::model()->find($criteria);
        if ($requisition!=null) {
            return $requisition->num;
        }
        else {
            throw new Exception("Requisition with id = ".$id." not found", 1);
        }
    }

    /**
     * Загрузка новый заявок из БД каталога
     * @return string $log - строка для вывода в консоль отчета о результатах загрузки
     */
    public static function loadRequisitionsFromCatalog() {
        $log = "";
        $sql="SELECT * FROM `usercart` WHERE status='".self::CART_STATUS_CHECKOUT."'";
        $command = Yii::app()->catalogDb->createCommand($sql);
        $userCarts=$command->queryAll();
        $userCartsId = array();

        foreach ($userCarts as $userCart) {
            $userCartsId[] = $userCart['id'];
            $cartProducts = json_decode($userCart['products']);
            $productIds = array();
            foreach ($cartProducts as $cartProduct) {
                $productIds[] = $cartProduct->id;
            }
            $productIdsStr = implode(",", $productIds);
            $productsSql = "SELECT * FROM `products` WHERE `id` IN (".$productIdsStr.") ORDER BY FIELD(id,".$productIdsStr.")";
            $command = Yii::app()->db->createCommand($productsSql);
            $products=$command->queryAll();

            $requisition = new Requisitions;            
            $requisition->status = "first";
            $requisition->new = 1;
            $requisition->from_catalog = 1;
            $requisition->save();

            foreach ($products as $index=>$product) {
                $reqProd = new ReqProds;
                $reqProd->requisition_id = $requisition->id;
                $reqProd->position_id = $index;
                $reqProd->position_num = $index+1;
                $reqProd->product_id = $product["id"];
                $reqProd->count = $cartProducts[$index]->count;
                $reqProd->list_type = "first";
                $reqProd->input_name = $product['original_name'];
                $reqProd->input_chod = $product['chod_display'];
                $reqProd->provider_id = $product['defaultprovider_id'];
                $reqProd->provprod_id = $product['defaultprovprod_id'];
                $reqProd->price_outsum = $cartProducts[$index]->price_catalog*$cartProducts[$index]->count;
                $res=$reqProd->save();

                Products::model()->increaseRating($product["id"]);
            }
            $log = "Added requisition #".$requisition->num." products count:".count($products);
        }

        if (count($userCarts)!=0) {
            $userCartsIdStr = implode(',', $userCartsId);
            $sql="UPDATE usercart SET status='".self::CART_STATUS_EXPORTED."' WHERE id IN (".$userCartsIdStr.")";
            $command = Yii::app()->catalogDb->createCommand($sql);
            $command->execute();
        }
        else {
            $log = "New requisitions not found.";
        } 

        return $log;       
    }




}